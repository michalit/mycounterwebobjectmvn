package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;


public class CntTest {

   @Test
    public void testD1() throws Exception {

        final int tempVar= new Cnt().d(5,0);
        assertEquals("d", Integer.MAX_VALUE , tempVar);

    }
 
   @Test
    public void testD2() throws Exception {

        final int tempVar= new Cnt().d(10,2);
        assertEquals("d",5 , tempVar);

            }
}
